FROM sandrokeil/bookdown

RUN apk -U --no-cache add \
    openssh-client \
    rsync
